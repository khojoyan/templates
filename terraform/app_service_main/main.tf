module "app_service" {
  source                = "../module/app_service"
  resource_group_name   = var.resource_group_name
  azurerm_linux_web_app = var.azurerm_linux_web_app
  location              = var.location
  sku_name              = var.sku_name
  os_type               = var.os_type
  passwd                = var.passwd
  url                   = var.url
  user                  = var.user
  azurerm_key_vault     = var.azurerm_key_vault
}
