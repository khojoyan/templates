terraform {
  backend "azurerm" {
    resource_group_name  = "CICD-Training"
    storage_account_name = "s3azure"
    container_name       = "s3azure"
    key                  = "terraform.tfstate"
  }
}