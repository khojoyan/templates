location              = "West Europe"
azurerm_linux_web_app = "Trial-Azure-App-Service"
resource_group_name   = "CICD-Training"
sku_name              = "S1"
os_type               = "Linux"
passwd                = "DockerRegistryPassword"
url                   = "DockerRegistryUrl"
user                  = "DockerRegistryUser"
azurerm_key_vault     = "KeyStorage-Az"