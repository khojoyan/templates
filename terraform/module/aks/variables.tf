variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "system_node_count" {
  type = number
}

variable "network" {
  type = string
}

variable "service_cidr" {}

variable "dns_service_ip" {}

variable "docker_bridge_cidr" {}

variable "vm_size" {}
variable "node_pool_type" {}

variable "default_node_pool_name" {
  type = string
}

variable "subnet_id" {}