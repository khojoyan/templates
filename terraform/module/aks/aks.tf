resource "azurerm_kubernetes_cluster" "aks" {
  name                = var.cluster_name
  location            = var.location
  resource_group_name = data.azurerm_resource_group.rg.name
  dns_prefix          = var.cluster_name

  default_node_pool {
    name                = var.default_node_pool_name
    node_count          = var.system_node_count
    vm_size             = var.vm_size
    vnet_subnet_id      = var.subnet_id
    type                = var.node_pool_type
    enable_auto_scaling = false
  }

  identity {
    type = "SystemAssigned"
  }

  network_profile {
    load_balancer_sku  = "standard"
    network_plugin     = "azure"
    network_policy     = "azure"
    service_cidr       =  var.service_cidr
    dns_service_ip     =  var.dns_service_ip
    docker_bridge_cidr =  var.docker_bridge_cidr
  }
}
