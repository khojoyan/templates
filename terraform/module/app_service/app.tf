resource "azurerm_service_plan" "pl" {
  name                = "${var.azurerm_linux_web_app}-plan"
  location            = var.location
  resource_group_name = data.azurerm_resource_group.rg.name
  sku_name            = var.sku_name
  os_type             = var.os_type
}

resource "azurerm_linux_web_app" "sv" {
  name                = var.azurerm_linux_web_app
  location            = var.location
  resource_group_name = data.azurerm_resource_group.rg.name
  service_plan_id     = azurerm_service_plan.pl.id

  site_config {
     application_stack {
        docker_image     =  "s3container.azurecr.io/python-hello"
        docker_image_tag = "1.0.0"
     }
  }

  app_settings                      = {
           "DOCKER_REGISTRY_SERVER_PASSWORD" = "${data.azurerm_key_vault_secret.passwd.value}" 
           "DOCKER_REGISTRY_SERVER_URL"      = "${data.azurerm_key_vault_secret.url.value}"
           "DOCKER_REGISTRY_SERVER_USERNAME" = "${data.azurerm_key_vault_secret.usern.value}" 
        }

  identity {
    type = "SystemAssigned"
  }

   logs {
           detailed_error_messages = false
           failed_request_tracing  = false

           http_logs {

               file_system {
                   retention_in_days = 0
                   retention_in_mb   = 70
                }
            }
        }
}
