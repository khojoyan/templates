variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "sku_name" {
  type    = string
}

variable "os_type" {
  type    = string
}

variable "azurerm_linux_web_app" {
  type = string
}
variable "passwd" {
  type = string
}

variable "url" {
  type  = string
}
variable "user" {
  type = string
}
variable "azurerm_key_vault" {
  type = string
}
