data "azurerm_resource_group" "rg" {
  name = var.resource_group_name
}

data "azurerm_key_vault" "s3" {
  name                = var.azurerm_key_vault
  resource_group_name = var.resource_group_name
}

data "azurerm_key_vault_secret" "passwd" {
  name = var.passwd
  key_vault_id = data.azurerm_key_vault.s3.id
}
data "azurerm_key_vault_secret" "url" {
  name = var.url
  key_vault_id = data.azurerm_key_vault.s3.id
}
data "azurerm_key_vault_secret" "usern" {
  name = var.user
  key_vault_id = data.azurerm_key_vault.s3.id
}