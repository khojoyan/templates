variable "location" {
  type    = string
}

variable "address_space" {}

variable "address_prefixes" {}

variable "network" {}

variable "resource_group_name" {
  type = string
}