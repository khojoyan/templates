resource "azurerm_network_security_group" "sg" {
  name                = "${var.network}-sg"
  location            = var.location
  resource_group_name = data.azurerm_resource_group.rg.name
}

resource "azurerm_virtual_network" "vn" {
  name                = "${var.network}-vn"
  location            = var.location
  resource_group_name = data.azurerm_resource_group.rg.name
  address_space       = var.address_space
}

resource "azurerm_subnet" "sb" {
  name                 = "${var.network}-sb"
  virtual_network_name = azurerm_virtual_network.vn.name
  resource_group_name  = data.azurerm_resource_group.rg.name
  address_prefixes     = var.address_prefixes
}

resource "azurerm_subnet_network_security_group_association" "associate" {
  subnet_id                 = azurerm_subnet.sb.id
  network_security_group_id = azurerm_network_security_group.sg.id
}