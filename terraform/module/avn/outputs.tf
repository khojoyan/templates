output "avn_name" {
  value = azurerm_virtual_network.vn.name
}

output "subnetwork_name"{
  value = azurerm_subnet.sb.name
}

output "resoure_group" {
  value = data.azurerm_resource_group.rg.name
}

output "subnetwork_id" {
  value = azurerm_subnet.sb.id
}