resource "azurerm_storage_account" "s3" {
  name                     = var.storage_name
  resource_group_name      = data.azurerm_resource_group.rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "staging"
  }
}

resource "azurerm_storage_container" "s3" {
  name                  = var.storage_name
  storage_account_name  = azurerm_storage_account.s3.name
  container_access_type = "private"
}