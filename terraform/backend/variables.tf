variable "location" {
  type    = string
  default = "West Europe"
}

variable "storage_name" {
  type    = string
  default = "s3azure"
}

variable "resource_group_name" {
  default = "CICD-Training"
  type    = string
}

variable "azurerm_container_registry" {
  type = string
  default = "s3container"
}

variable "sku" {
  type = string
  default = "Basic"
}