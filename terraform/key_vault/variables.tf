variable "location" {
  type    = string
  default = "West Europe"
}
variable "resource_group_name" {
  type    = string
  default = "CICD-Training"
}
variable "azurerm_key_vault" {
  type    = string
  default = "KeyStorage-Az"
}

variable "sku_name" {
  type    = string
  default = "standard"
}