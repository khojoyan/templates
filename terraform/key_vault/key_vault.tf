resource "azurerm_key_vault" "s3" {
  name                        = var.azurerm_key_vault
  location                    = var.location
  resource_group_name         = var.resource_group_name
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  enabled_for_disk_encryption = true

  sku_name = var.sku_name

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Get",
    ]

    secret_permissions = [
      "Get", "Backup", "Delete", "List", "Purge", "Recover", "Restore", "Set"
    ]

    storage_permissions = [
      "Get",
    ]
  }
}

resource "azurerm_key_vault_secret" "passwd" {
  name         = "DockerRegistryPassword"
  value        = "paswdforazure"
  key_vault_id = azurerm_key_vault.s3.id
}
resource "azurerm_key_vault_secret" "url" {
  name         = "DockerRegistryUrl"
  value        = "https://s3container.azurecr.io"
  key_vault_id = azurerm_key_vault.s3.id
}
resource "azurerm_key_vault_secret" "usern" {
  name         = "DockerRegistryUser"
  value        = "s3container"
  key_vault_id = azurerm_key_vault.s3.id
}