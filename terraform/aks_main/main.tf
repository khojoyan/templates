module "avn" {
  source              = "../module/avn"
  resource_group_name = var.resource_group_name
  location            = var.location
  network             = var.network
  address_space       = var.address_space
  address_prefixes    = var.address_prefixes
}

module "aks" {
  source                 = "../module/aks"
  resource_group_name    = var.resource_group_name
  cluster_name           = var.cluster_name
  location               = var.location
  system_node_count      = var.system_node_count
  network                = module.avn.avn_name
  subnet_id              = module.avn.subnetwork_id
  dns_service_ip         = var.dns_service_ip
  node_pool_type         = var.node_pool_type
  default_node_pool_name = var.default_node_pool_name
  service_cidr           = var.service_cidr
  docker_bridge_cidr     = var.docker_bridge_cidr
  vm_size                = var.vm_size
}
